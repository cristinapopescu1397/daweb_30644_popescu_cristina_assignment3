import React from 'react';
import {Jumbotron} from 'reactstrap';
import carousel1 from '../images/carusel1.png';
import carousel2 from '../images/carusel2.png'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
const car = {
    height: "480px"
};
const cul = {
    color: "#d2c7d4"
};
class Home extends React.Component {
    render() {
        let content = {

            EN: {
                title: "Indoor Navigation System",
                description:
                    "Indoor navigation deals with navigation within buildings. Because GPS reception is normally non-existent inside buildings, other positioning technologies are used here when automatic positioning is desired. Wi-Fi or beacons (Bluetooth Low Energy, BLE) are often used in this case to create a so-called `indoor GPS`."
            },

            RO: {
                title: "Sistem de Navigare Interioară",
                description:
                    "Navigarea în interior se ocupă de navigarea în clădiri. Deoarece recepția GPS este inexistentă în mod normal în interiorul clădirilor, alte tehnologii de poziționare sunt utilizate aici atunci când se dorește poziționarea automată. Wi-Fi sau balize (Bluetooth Low Energy, BLE) sunt deseori utilizate în acest caz pentru a crea un așa-numit `GPS interior`."
            }

        };
        localStorage.getItem("language") === "RO" ? (content = content.RO) : (content = content.EN);
        return (
            <div>
                <Jumbotron>
                    <h3>{content.title}</h3>

                    <p style={cul}> <b>{content.description}</b></p>
                    <Carousel dynamicHeight="true">
                        <div style={car}>
                            <img align="middle" src={carousel1}  alt="sd"/>
                        </div>
                        <div style={car}>
                            <img src={carousel2} alt="dd"/>
                        </div>
                    </Carousel>
                </Jumbotron>
            </div>
        )
    };
}

export default Home
