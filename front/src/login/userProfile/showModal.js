import React from 'react'
import Modal from 'react-bootstrap/Modal';
import ControlledTabs from './changeDataModal';

export default function ShowModal(props){
    return (
        <>
            <Modal {...props} >
                <Modal.Header closeButton>
                    <Modal.Title>Edit profile</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ControlledTabs/>
                </Modal.Body>
            </Modal>
        </>
    );
}